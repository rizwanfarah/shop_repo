-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2019 at 05:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `country_list`
--

-- --------------------------------------------------------

--
-- Table structure for table `city_data`
--

CREATE TABLE IF NOT EXISTS `city_data` (
`userno` int(11) NOT NULL,
  `country` text NOT NULL,
  `state` text NOT NULL,
  `city` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_data`
--

INSERT INTO `city_data` (`userno`, `country`, `state`, `city`) VALUES
(1, 'india', 'maharashtra', 'nagpur'),
(2, 'australia', 'sydney', 'perth'),
(3, 'pakistan', 'lahor', 'karachi'),
(4, 'england', 'london', 'vizag'),
(5, 'bangladesh', 'dhaka', 'mirpur'),
(6, 'shrilanka', 'colombo', 'pallekele'),
(7, 'zimbambe', 'harare', 'tiger'),
(8, 'netherland', 'melbourn', 'singapur'),
(9, 'brazil', 'dhurve', 'hongo'),
(10, 'hongkong', 'xyz', 'abc'),
(11, 'india', 'maharashtra', 'pune'),
(12, 'india', 'maharashtra', 'goa'),
(13, 'australia', 'hobart', 'berline'),
(14, 'pakistan', 'ranchi', 'kotla');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city_data`
--
ALTER TABLE `city_data`
 ADD PRIMARY KEY (`userno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city_data`
--
ALTER TABLE `city_data`
MODIFY `userno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
